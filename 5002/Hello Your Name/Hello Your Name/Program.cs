﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Your_Name
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Greetings, what is your name ?");
            String myName = Console.ReadLine();
            Console.WriteLine($"Thank you {myName}");
        }
    }
}
