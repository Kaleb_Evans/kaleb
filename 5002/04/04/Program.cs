﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 1;
            var b = 2;

            Console.WriteLine("");

            Console.WriteLine($"  A is equal to {a}");
            Console.WriteLine("");
            Console.WriteLine($"  B is equal to {b}");
            Console.WriteLine("");
            Console.WriteLine($"  A * B = {a*b}");
            Console.WriteLine("");
        }
    }
}
