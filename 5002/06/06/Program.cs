﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, please enter 1 to convert KM to Miles");

            int type = Convert.ToInt32(Console.ReadLine());

            if (type == 1)

            {
                {
                    Console.Clear();
                    Console.WriteLine("KM to Mile converter.");
                    Console.WriteLine();
                    Console.WriteLine("Please enter a number.");

                    int km = Convert.ToInt32(Console.ReadLine());
                    const double mile = 0.6213;

                    Console.WriteLine($"{km} kms is {km * mile} miles");
                }
            }
        }
    }
}
    

